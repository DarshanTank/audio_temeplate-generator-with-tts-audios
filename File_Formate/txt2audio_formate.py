import json
import re

# with open("./1.json","r",encoding="utf-8") as f:
#     template = json.load(f)

def get_dynamic_variables(template):
    regex = "<\w*>"
    dynamic_variable_list = []

    for key in template:
        txt =  template[key]
        variable_list = re.findall(regex, txt)
        dynamic_variable_list.extend(variable_list)

    return list(set(dynamic_variable_list))
# 2.json
def txt2txt_audio_formate(template:json):
    regex = "<\w*>"
    splitted_template = {}

    for key in template:
        
        txt =  template[key]
        dynamic_variable_list = re.findall(regex, txt)
        
        try:
            if dynamic_variable_list != []:
                regex_start = dynamic_variable_list[0]
                if len(dynamic_variable_list) == 1:
                    if re.search(f"{regex_start}$",txt):
                        dynamic_variable_list.clear()

                if re.search(f"^{regex_start}",txt):
                    dynamic_variable_list.pop(0)

            if dynamic_variable_list == []:
                splitted_template[key] = template[key]
        except Exception as e:
            print("error while extraction")

        
        for num, dynamic_variable in enumerate(dynamic_variable_list):
            
            try:
                txt_list = txt.split(dynamic_variable)
                txt_new = txt_list[0].strip() + " " +str(dynamic_variable)
                alpha = chr(97 + num)
                key_name = f"{key}_{alpha}"

                splitted_template[key_name] = txt_new
                txt = txt_list[1]

                if num == len(dynamic_variable_list)-1:
                    alpha = chr(97 + num + 1)
                    key_name = f"{key}_{alpha}"

                    if txt_list[1]:
                        splitted_template[key_name] = txt_list[1].strip()
            except Exception as e:
                print("error while naming")

    # print(splitted_template)
    # with open("./2.json","w+",encoding="utf8") as f:
    #     json.dump(splitted_template,f,ensure_ascii=False)

    return splitted_template

# 3.json
def create_audio_template(template:json):
    regex = "<\w*>"

    d = {}

    for t,s in template.items():
        ls1 = []
        ls = re.findall(regex,s)
        
        # print(f"dynamic variables are {ls}")
        
        if ls == []:
            d[t] = [f"{t}.wav"]
            continue
        
        elif len(ls) == 1:
            if re.search(f"{ls[0]}$",s):
                d[t] = [f"{t}.wav {ls[0]}"]
                continue

            if re.search(f"^{ls[0]}",s):
                d[t] = [f"{ls[0]} {t}.wav"]
                continue
        else:
            if re.search(f"^{ls[0]}",s):
                ls1.append(ls[0])
                ls.pop(0)
                

        for n,i in enumerate(ls):
            a = s.split(i,1)[0]
            key = f"{t}_{chr(97+n)}.wav"
            if a:
                ls1.append(key)
            ls1.append(i)
            # print(f"stage wise list {ls1}")
            if n == len(ls)-1:
                if s.split(i)[1] not in [""," "]:
                    # print(s.split(i)[1])
                    key = f"{t}_{chr(97+n+1)}.wav"
                    # print(key)
                    ls1.append(key)
            s = s.split(i,1)[1]
            # print(f"string to be processed is ******************{s}")
        d[t] = ls1
        

    # print(d)
    for i in d:
        d[i] = " ~ ".join(d[i])

    # print(d)

    for k,v in template.items():
        if "EOC" in v:
            # print(k)
            d[k] = d[k] + " ~ silence.wav ~ EOC"

    with open("./audio_template.json","w",encoding="utf8") as f:
        json.dump(d,f,ensure_ascii=False)
    return 1
