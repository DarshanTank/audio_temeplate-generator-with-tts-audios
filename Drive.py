from TTS.text2audio import process_text
import re
import json
from File_Formate.txt2audio_formate import txt2txt_audio_formate,create_audio_template, get_dynamic_variables
from File_Formate.validate_audio import validate_audio_template

#  import template file

def process(input_file_path,language):
    # input_file_path = "./1.json"
    # language = "english"
    with open(input_file_path,"r",encoding="utf-8") as f:
        template = json.load(f)

    list_dynamic_vars = get_dynamic_variables(template)
    j1 = txt2txt_audio_formate(template)

    # This is for text to audio.........................................

    def pre_process_text(text):
        for i in list_dynamic_vars:
            if i in text:
                text = text.replace(i,"")
        text = text.replace("~","")
        text = text.replace("  "," ")
        return text

    for k,v in j1.items():
        # language = "english"
        text = pre_process_text(v)      #"hey my name is darshan"
        output_file_name = k            #"inital_msg_a"
        process_text(text,language, output_file_name)


    create_audio_template(template)
    output_audio_file_path = "./result"
    output_audio_template_path = "./audio_template.json"
    validate_audio_template(output_audio_file_path,output_audio_template_path)
