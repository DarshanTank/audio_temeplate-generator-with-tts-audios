import numpy as np
from scipy.io.wavfile import write
import json
import requests
from multiprocessing import Pool
import time
import uuid
import wave
import os


url = "https://vb.gnani.site/tts"
sampling_rate=16000
# headers = {
#         "lang": 'en-IN',
#         "version": "v2"
#     }

language_mapping ={
    "english": 'en-IN',
    "hindi": 'hi-IN',
    "kannada": 'kn-IN',
    "telugu": 'te-IN',
    "marathi": 'mr-IN',
    "tamil":'ta-IN',
    "gujarati": 'gu-IN'
}
def process_text(line,language, output_file_name):
    #print(line)
    line = line.strip()
    language = language.lower()
    language_code = language_mapping[language]
    print(language_code)
    if language_code == None:
        return "Enter proper language"
    data = {
                "uuid": str(uuid.uuid4()),
                "text": line,
                'sampling_rate': sampling_rate,
                'alpha': 1.0,
                'gain': 0.0,
                'transliteration_backend': 'microsoft'
            }
    headers = {
        "lang": language_code,
        "version": "v2"
    }
    starttime = time.time()
    response = requests.post(url, headers=headers, json = data, verify=True)
    print("time taken is for : ",line," is :",time.time()-starttime)
    print(response.text)
    out = json.loads(response.text)
    print(response.status_code)
    if not os.path.isdir("./result"):
        os.makedirs("result")
    if response.status_code == 200:
        audio = np.asarray(out["audio"],dtype=np.int16)
        waveFile = wave.open(f'result/{output_file_name}.wav', 'wb')
        waveFile.setnchannels(1)
        waveFile.setsampwidth(2)
        waveFile.setframerate(16000)
        waveFile.writeframes(audio)
        waveFile.close()
        #write('./hindi.wav',16000,audio)

# # l1 = "कोई बात नहीं, कृपया हमे बताये की आप कब पेमेंट करेंगे "
# # lang = "hindi"
# l1 = "કોઈ વાંધો નથી, કૃપા કરીને અમને જણાવો કે તમે ક્યારે ચૂકવણી કરશો"
# lang = "gujarati"
# # l1 = "माझे नाव आहे आणि माझे वय आहे आणि मी येथे काम करतो"
# # lang = "marathi"

# # l1 = "மாசத்துக்கான  உங்க"
# # lang = "tamil"

# # l1 = "నా పేరు మరియు నా వయస్సు మరియు నేను లో పని చేస్తున్నాను"
# # lang = "telugu"
# out_f = "temp"
# process_text(l1,lang,out_f)
