import os
import re
# import boto3
import json

# path= "/home/sagar/Downloads/audios"
# template_file = "3.json"

def local_audio_files(path):
    audio_list = os.listdir(path)
    list(audio_list).sort()
    print("\n audio files in folder \n",audio_list)
    return audio_list

def get_template_audio_files(template_file_path):
    
    with open(template_file_path, "r", encoding="utf-8") as temp:
        data = json.load(temp)
    values=list(data.values())
    template_audio_files = []
    for value in values:
        template_audio_files.extend(re.findall('[a-zA-Z0-9_]+.wav', value))
    template_audio_files = list(set(template_audio_files))
    template_audio_files.sort()
    print("\n audio files in templates\n",template_audio_files)
    return template_audio_files


def validate_audio_template(audio_file_path, template_file_path):   
    local_audio = local_audio_files(audio_file_path)
    template_audio =  get_template_audio_files(template_file_path)
    for i in template_audio:
        if i not in local_audio:
            print(i,"  ---> is not present in local audios")

    for i in local_audio:
        if i not in template_audio:
            print(i," ====> is not present in tempalte file")